module Shwf.Server.Validation ( Validatable(..)
                              , VError(..)
                              ) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC

import Data.Aeson as JSON
import GHC.Generics

-- ValidationError
data VError = VError String
            | VErrorFromStrNotImplemented
            | VErrorValidationNotImplemented
            | VErrorInvalidChar
            | VErrorInvalidUTF8
            | VErrorInvalidRestricted
            | VErrorInvalidHex
            | VErrorInvalidRealFloat
            | VErrorInvalidEmail String -- error message
            | VErrorReadFailed String -- https://hackage.haskell.org/package/base-4.17.0.0/docs/Text-Read.html#v:readEither returned Left
            | VErrorTooSmall Int Int -- min got
            | VErrorTooBig Int Int -- max got
            | VErrorWrongSize Int Int -- expected got
            | VErrorMissingRequiredField String
            | VErrorNothing
  deriving          (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

class Validatable a where
  fromBS :: BS.ByteString -> Either VError a
  fromBS bs | not (BS.isValidUtf8 bs) = Left VErrorInvalidUTF8
            | otherwise            = fromStr $ BSC.unpack bs

  fromStr :: String -> Either VError a
  fromStr _ = Left VErrorFromStrNotImplemented

  mayFromStr :: String -> Maybe a
  mayFromStr str = case fromStr str of
    Left _ -> Nothing
    Right res -> Just res

  fromMayStr :: Maybe String -> Either VError a
  fromMayStr (Just str) = fromStr str
  fromMayStr Nothing = Left VErrorNothing

  fromMayBS :: Maybe BS.ByteString -> Either VError a
  fromMayBS (Just bs) = fromBS bs
  fromMayBS Nothing = Left VErrorNothing

  validate :: a -> Either VError a

  {-# MINIMAL (validate) | (validate, fromStr) #-}

