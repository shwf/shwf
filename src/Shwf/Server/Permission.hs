module Shwf.Server.Permission ( Permission(..)
                              , hasPermission
                              ) where

import Data.String ( IsString(..) )
import Data.Text (pack, unpack)
import Data.List.Split (splitOn)

import Data.Aeson (FromJSON, ToJSON, Value(String), toJSON, parseJSON, withText)

--import GHC.Generics

-- user:view
-- user:create:normal
-- user:create:admin
-- user:delete
-- admin:log:view
--
-- user:create:*
-- admin:*

data Permission = PermPart String Permission  | PermWildcard | PermStop
--  deriving (Generic)

instance Show Permission where
  show perm = permToString perm
instance ToJSON Permission where
  toJSON perm = String $ pack $ permToString perm
instance FromJSON Permission where
  parseJSON = withText "Permission" $ \v -> do
    return $ fromString $ unpack $ v

instance IsString Permission where
  fromString :: String -> Permission
  fromString str = fromString' $ splitOn ":" str
    where
      fromString' :: [String] -> Permission
      fromString' (x:xs) | x == "*"  = PermWildcard
                     | x == ""   = PermStop
                     | otherwise = PermPart x (fromString' xs)
      fromString' [] = PermStop

permToString :: Permission -> String
permToString (PermPart x PermStop) = x
permToString (PermPart x rest) = x ++ ":" ++ show rest
permToString PermStop = ""
permToString PermWildcard = "*"

-- The first permission is the Testpermission which is the permission needed to do something.
-- The second permission is one of the users permissions.
--                  Testpermission One of Userpermissions
isPermissionGranted :: Permission -> Permission -> Bool
isPermissionGranted PermWildcard   _              = False -- Testpermission is not allowed to include wildcards
isPermissionGranted _              PermWildcard   = True
isPermissionGranted PermStop       PermStop       = True
isPermissionGranted PermStop       (PermPart _ _) = False
isPermissionGranted (PermPart _ _) PermStop       = False
isPermissionGranted (PermPart a x) (PermPart b y) | a == b    = isPermissionGranted x y
                                                  | otherwise = False


hasPermission :: [Permission] -> Permission -> Bool
hasPermission [x] perm = isPermissionGranted perm x
hasPermission (x:xs) perm = hasPermission [x] perm || hasPermission xs perm
hasPermission [] _ = False
