module Shwf.Server.Routing ( Route(..)
                           , ParserID
                           , Parser
                           , OperationID
                           , Operation(..)
                           --, Guard (..)
                           --, RespStatus (..)
                           ) where

import Shwf.Server.Validation
import Shwf.Server.Validation.String
import Shwf.Server.Permission
import Shwf.Server.Session
import Shwf.Server.Templates


import qualified Happstack.Server as HS

import Data.Aeson ( ToJSON
                  , FromJSON
                  , Value
                  , decode
                  )

import Data.Functor ((<&>))
import qualified Data.HashMap.Strict as HM

import qualified Data.ByteString.Lazy.Char8 as C

-- |
type ParserID = String
type Parser = String -> Maybe Value
type Parsers =  HM.HashMap ParserID Parser

parse :: (FromJSON a) => Parsers -> ParserID -> String -> Maybe a
parse parsers pn str = (HM.findWithDefault (\_ -> Nothing) pn parsers) str

--- BEISPIEL

sampleParser :: Parsers
sampleParser = HM.fromList [
      ("json", decode . C.pack)
    -- , ("restricetd", mayFromStr @RestrictedString)
    , ("self", Just . Prelude.id)
  ]


{-
data Guard = Method HS.Method
           | LoggedIn
           | HasPerm [Permission]
-}


--data RespStatus = Ok
--                | NotFound
--                | BadRequest
--                | InternalServerError
--                | SeeOther String

type OperationID = String
--data Operation = SimpleOperation TemplateName (forall a. (ToJSON a) =>  HS.Request -> Session (RespStatus, a))
data Operation = SimpleOperation TemplateName (forall a. (ToJSON a) =>  HS.Request -> Session a)
               -- | DirOperation -- TODO serve directory
               -- | PandocOperation -- TODO serve directory but with pandoc

--handlers :: [(OperationID, [Guard])]
--handlers = [
--  ("user", GET, "json")
--]


-- |Defines the routing.
data Route = Use OperationID -- ^ Operation to use for this route
           | Dir String [Route] -- ^ Denotes a "directory" in the resource path. The String is the name of the directory. Example: "users" matches /users/
           | NullDir [Route] -- ^ Denotes the end of the url (/)
           | Path StorageKey ParserID [Route] -- ^ Denotes a variable path in the resource path. The StorageKey denotes where the value should be stored in the session storage. The Parser parses the string from the url to its native type. Example: "userid" matches /<userid>
           | Parameter -- ^ Store a url parameters value. Example: ?foo=bar
               String -- ^ Key of the parameter in the url.
               StorageKey -- ^ Where to store the value in the session storage.
               ParserID -- ^ Parser for the parameter.
               (Maybe String) -- ^ Default value. Parameter is required if no default is given.
               [Route]
           | Cookie String StorageKey ParserID (Maybe String) [Route] -- ^ Denotes a Cookie in the request. Arguments are similar to those of Parameter.
           -- | StoreSomething StorageKey (forall a. a -> IO (Maybe String)) [Route]


resolveRouting :: Route -> HS.Request -> Maybe (OperationID, Storage, [String])
resolveRouting _ _ = Nothing

resolveRouting'
  :: HS.Request
  -> Parsers
  -> Storage
  -> [Route] -- ^ The routing information.
  -> [String] -- ^ The remaining uri, split on /, and then decoded (rqPaths)
  -> Maybe (OperationID, Storage, [String])
resolveRouting' req par sto [Use hn] ps = Just (hn, sto, ps)
resolveRouting' req par sto [Dir dir routes] (p:ps) | p == dir  = resolveRouting' req par sto routes ps
                                                    | otherwise = Nothing
resolveRouting' req par sto [Dir dir routes] []     = Nothing
resolveRouting' req par sto [NullDir routes] (p:ps) = Nothing
resolveRouting' req par sto [NullDir routes] []     = resolveRouting' req par sto routes []
--resolveRouting' req par sto [Path key pn routes] (p:ps) = case parse par pn p <&> (\value -> storeDirect sto key value) of
--  Nothing -> Nothing
--  Just nsto ->  resolveRouting' req par nsto routes ps



routes :: [Route]
routes = [
    Dir "users" [ Use "Yeet" ]
    --Var "test" (Maybe (\x -> isRight $ (fromStr x :: Either VError RestrictedString)))
  ]

