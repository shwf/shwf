module Shwf.Server.DB ( DB(..)
                      , DBKey(..)
                      , DBSetKey(..)
                      , DBGroupKey
                      , DBError(..)
                      , Redis(..)
                      --, fromDBConfig
                      ) where

import Shwf.Server.DB.Types
import Shwf.Server.DB.Redis
import Shwf.Server.Config ( DBConfig(..) )

import Data.Aeson ( ToJSON
                  , FromJSON
                  )

class DB d where
  dbInit :: d -> a -> IO (Either DBError a)
  dbTest :: d -> IO (Either DBError ())
  dbClose :: d -> IO (Either DBError ())

  dbStore :: ToJSON a => d -> Maybe DBGroupKey -> DBKey -> a -> IO (Either DBError a)
  dbCache :: ToJSON a => d -> Integer ->  DBKey -> a -> IO (Either DBError a)
  dbFetch :: FromJSON a => d -> DBKey -> IO (Either DBError a)
  dbDelete :: d -> DBKey -> IO (Either DBError Bool)

  dbSetAdd :: d -> DBSetKey -> String -> IO (Either DBError Bool)
  dbSetRemove :: d -> DBSetKey -> String -> IO (Either DBError Bool)
  dbSetDelete :: d -> DBSetKey -> IO (Either DBError Bool)
  dbSetMembers :: d -> DBSetKey -> IO (Either DBError [String])
  dbSetContains :: d -> DBSetKey -> String -> IO (Either DBError Bool)

  dbFetchAll :: FromJSON a => d -> DBGroupKey -> IO (Either DBError [a])
  dbFetchAll db groupkey = do
    dbSetMembers db groupkey >>= \case
      Left err -> return $ Left err
      Right keys -> (mapM (\k -> dbFetch db (DBKey k)) keys) >>= return . sequence 
      --Right keys -> (mapM (\k -> dbFetch db (DBKey k)) keys :: forall a1. FromJSON a1 => IO [Either DBError a1]) >>= return . sequence 



instance DB DBConfig where
  dbInit        (DBRedisConfig p c) = redisInit        Redis{prefix=p, connInfo=c}
  dbTest        (DBRedisConfig p c) = redisTest        Redis{prefix=p, connInfo=c}
  dbClose       (DBRedisConfig p c) = redisClose       Redis{prefix=p, connInfo=c}
  dbStore       (DBRedisConfig p c) = redisStore       Redis{prefix=p, connInfo=c}
  dbCache       (DBRedisConfig p c) = redisCache       Redis{prefix=p, connInfo=c}
  dbFetch       (DBRedisConfig p c) = redisFetch       Redis{prefix=p, connInfo=c}
  dbDelete      (DBRedisConfig p c) = redisDelete      Redis{prefix=p, connInfo=c}
  dbSetAdd      (DBRedisConfig p c) = redisSetAdd      Redis{prefix=p, connInfo=c}
  dbSetRemove   (DBRedisConfig p c) = redisSetRemove   Redis{prefix=p, connInfo=c}
  dbSetDelete   (DBRedisConfig p c) = redisSetDelete   Redis{prefix=p, connInfo=c}
  dbSetMembers  (DBRedisConfig p c) = redisSetMembers  Redis{prefix=p, connInfo=c}
  dbSetContains (DBRedisConfig p c) = redisSetContains Redis{prefix=p, connInfo=c}


--fromDBConfig :: forall (DB d). DBConfig -> d
--fromDBConfig (DBRedisConfig p c)  = Redis{prefix=p, connInfo=c}
----fromDBConfig (DBMySQLConfig c)    = MySQL{connInfo=c}
----fromDBConfig (DBSQLiteConfig f) = SQLite{file=f}



