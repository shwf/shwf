module Shwf.Server.DB.Types ( DBKey(..)
                            , DBSetKey(..)
                            , DBGroupKey
                            , DBError(..)
                            ) where

import Data.String( IsString(..) )

import Data.Aeson ( Value )

newtype DBKey = DBKey String
  deriving newtype (IsString)

newtype DBSetKey = DBSetKey DBKey
  deriving newtype (IsString)

type DBGroupKey = DBSetKey

data DBError = DBError String
             | DBKeyNotFoundError (Either DBKey DBSetKey)
             | DBJSONDecodeError String (Maybe Value)
             | DBUnknownError



