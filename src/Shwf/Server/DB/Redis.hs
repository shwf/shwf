module Shwf.Server.DB.Redis ( Redis(..)
                            , redisInit
                            , redisTest
                            , redisClose
                            , redisStore
                            , redisCache
                            , redisFetch
                            , redisDelete
                            , redisSetAdd
                            , redisSetDelete
                            , redisSetRemove
                            , redisSetMembers
                            , redisSetContains
                            ) where

import Shwf.Server.DB.Types

import Data.Functor ( (<&>) )

import qualified Data.ByteString as BS
import qualified Data.ByteString.UTF8 as BSU

import qualified Database.Redis as R

import Data.Aeson ( ToJSON
                  , FromJSON
                  , encode
                  , decode
                  , eitherDecode
                  )


data Redis = Redis
  { prefix :: String
  , connInfo :: R.ConnectInfo
  }


-- https://mmhaskell.com/real-world/redis
runRedisAction :: Redis -> R.Redis a -> IO a
runRedisAction r action = do
  conn <- R.connect $ connInfo r
  res <- R.runRedis conn action
  R.disconnect conn
  return res

ereplyToDBErr :: Either R.Reply a -> Either DBError a
ereplyToDBErr (Left (R.Error bs)) = Left $ DBError $ BSU.toString bs
ereplyToDBErr (Left _)            = Left $ DBUnknownError
ereplyToDBErr (Right res)         = Right res

-- Build normal key
nkey :: Redis -> DBKey -> BS.ByteString
nkey r (DBKey k) = BSU.fromString $ prefix r ++ ":n:" ++ k

-- Build set key
skey :: Redis -> DBSetKey -> BS.ByteString
skey r (DBSetKey (DBKey k)) = BSU.fromString $ prefix r ++ ":s:" ++ k

redisInit :: Redis -> a -> IO (Either DBError a)
redisInit r val = redisTest r >>= (\_ -> return $ Right val)

redisTest :: Redis -> IO (Either DBError ())
redisTest r = runRedisAction r $ do
  res <- R.ping <&> ereplyToDBErr
  return $ res <&> (\_ -> ())


redisClose :: Redis -> IO (Either DBError ())
redisClose _ = return $ Right ()


redisStore :: ToJSON a => Redis -> Maybe DBGroupKey -> DBKey -> a -> IO (Either DBError a)
redisStore r Nothing key value = runRedisAction r $ do
  res <- R.set (nkey r key) (BS.toStrict $ encode value) <&> ereplyToDBErr
  return $ res <&> (\_ -> value)
redisStore r (Just groupkey) key@(DBKey keystr) value = do
  redisSetAdd r groupkey keystr >>= \case
    Left err -> return $ Left err
    Right _ -> redisStore r Nothing key value



  

--                                 ttl in sec
redisCache :: ToJSON a => Redis -> Integer -> DBKey -> a -> IO (Either DBError a)
redisCache r ttl key value = runRedisAction r $ do
  res <- R.setex (nkey r key) ttl (BS.toStrict $ encode value) <&> ereplyToDBErr
  return $ res <&> (\_ -> value)

redisFetch :: FromJSON a => Redis -> DBKey -> IO (Either DBError a)
redisFetch r key = runRedisAction r $ do
  R.get (nkey r key) <&> ereplyToDBErr >>= \case
    Left err -> return $ Left err
    Right Nothing -> return $ Left $ DBKeyNotFoundError (Left key)
    Right (Just raw) -> case eitherDecode bs of
      Left err -> return $ Left $ DBJSONDecodeError err (decode bs)
      Right res -> return $ Right res
      where bs = BS.fromStrict raw

--                                                  true if element deleted
redisDelete :: Redis -> DBKey -> IO (Either DBError Bool)
redisDelete r key = runRedisAction r $ do
  res <- R.del [(nkey r key)] <&> ereplyToDBErr
  return $ res <&> (\num -> num >= 1)

--                                                               true if element is new
redisSetAdd :: Redis -> DBSetKey -> String -> IO (Either DBError Bool)
redisSetAdd r key value = runRedisAction r $ do
  res <- R.sadd (skey r key) [BSU.fromString value] <&> ereplyToDBErr
  return $ res <&> (\num -> num >= 1)

--                                                                  true if element was in the set and is now removed
redisSetRemove :: Redis -> DBSetKey -> String -> IO (Either DBError Bool)
redisSetRemove r key value = runRedisAction r $ do
  res <- R.srem (skey r key) [BSU.fromString value] <&> ereplyToDBErr
  return $ res <&> (\num -> num >= 1)

redisSetDelete :: Redis -> DBSetKey -> IO (Either DBError Bool)
redisSetDelete r key = runRedisAction r $ do
  res <- R.del [(skey r key)] <&> ereplyToDBErr
  return $ res <&> (\num -> num >= 1)

redisSetMembers :: Redis -> DBSetKey -> IO (Either DBError [String])
redisSetMembers r key = runRedisAction r $ do
  res <- R.smembers (skey r key) <&> ereplyToDBErr
  return $ res <&> (map BSU.toString)

redisSetContains :: Redis -> DBSetKey -> String -> IO (Either DBError Bool)
redisSetContains r key value = runRedisAction r $ do
  res <- R.sismember (skey r key) (BSU.fromString value) <&> ereplyToDBErr
  return res




