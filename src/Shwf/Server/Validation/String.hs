module Shwf.Server.Validation.String ( RestrictedString , pattern PRestrictedString
                                     ) where

import Shwf.Server.Validation

import Data.Char

import Data.Aeson as JSON
import GHC.Generics

import Text.Blaze.Html5 (ToValue(..))

-- Restricted UTF-8 String (No Control Characters or other weird stuff)
newtype RestrictedString = RestrictedString String
  deriving          (Generic, Eq)
  deriving anyclass (ToJSON, FromJSON)
  deriving newtype  (ToValue)
instance Validatable RestrictedString where
  fromStr str = validate $ RestrictedString str
  validate resstr@(RestrictedString str) | any isControl     str  = Left VErrorInvalidRestricted -- forbid non-printing characters of the Latin-1 subset of Unicode
                                         | any isMark        str  = Left VErrorInvalidRestricted -- forbid 'combining' characters
                                         | any isInForbiddenGeneralCategory str  = Left VErrorInvalidRestricted -- forbid reverse characters and similiar
                                         | not (all isValidRestrictedLetter str) = Left VErrorInvalidRestricted -- only allow valid letters
                                         | otherwise = Right resstr
pattern PRestrictedString :: String -> RestrictedString
pattern PRestrictedString str = RestrictedString str
{-# COMPLETE PRestrictedString #-}
instance Show RestrictedString where
  show (RestrictedString str) = str

isValidRestrictedLetter :: Char -> Bool
isValidRestrictedLetter c | c == '-' = True
                        | isDigit c = True
                        | isSymbol c = True
                        | isSpace c = True
                        | isLetter c = True
                        | isAlphaNum c = True
                        | isPrint c = True
                        | otherwise = False

isInForbiddenGeneralCategory :: Char -> Bool
isInForbiddenGeneralCategory c = isForbiddenGeneralCategory $ generalCategory c

isForbiddenGeneralCategory :: GeneralCategory -> Bool
isForbiddenGeneralCategory Format = True
isForbiddenGeneralCategory PrivateUse = True
isForbiddenGeneralCategory NotAssigned = True
isForbiddenGeneralCategory _ = False



{-
validateRestrictedUtf8String :: BS.ByteString -> Either VError String
validateRestrictedUtf8String unvalidatedbs
isValidRestrictedUtf8String uname | len > 20  = False
                         | len < 2   = False
                         | any isControl     uname  = False -- forbid non-printing characters of the Latin-1 subset of Unicode
                         | any isMark        uname  = False -- forbid 'combining' characters
                         | not (all isValidUsernameLetter uname) = False -- only allow valid letters
                         | otherwise = True
                         where len = length uname
-}
