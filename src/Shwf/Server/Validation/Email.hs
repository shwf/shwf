module Shwf.Server.Validation.Email ( Email , pattern PEmail
                                    ) where

import Shwf.Server.Validation

import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC

import Data.Aeson as JSON
import Data.Aeson.Types (Parser)
import GHC.Generics

import qualified Text.Email.Validate as EV

--              localPart domainPart
data Email = Email String String
  deriving (Generic)
instance Validatable Email where
  fromStr str = case EV.validate (BSC.pack str) :: Either String EV.EmailAddress of
    Left err -> Left $ VErrorInvalidEmail err
    Right mail -> validate $ Email (BSC.unpack $ EV.localPart mail) (BSC.unpack $ EV.domainPart mail)
  validate resmail@(Email local domain) = case EV.validate (BS.concat [BSC.pack local, BSC.singleton '@', BSC.pack domain]) :: Either String EV.EmailAddress of
    Left err -> Left $ VErrorInvalidEmail err
    Right _ -> Right $ resmail
instance Show Email where
  show (Email local domain) = local ++ "@" ++ domain
instance ToJSON Email where
  toJSON mail = JSON.String $ T.pack $ show mail
instance FromJSON Email where
  parseJSON = withText "Email" $ mapEmailParser . fromStr . T.unpack
    where
      mapEmailParser :: Either VError Email -> Parser Email
      mapEmailParser (Left err) = fail $ show err
      mapEmailParser (Right mail) = return mail
pattern PEmail :: String -> String -> Email
pattern PEmail local domain = Email local domain
{-# COMPLETE PEmail #-}

