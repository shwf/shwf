module Shwf.Server.Validation.HexString ( HexString , pattern PHexString
                                        , HexString512 , pattern PHexString512
                                        , HexString256 , pattern PHexString256
                                        , HexString128 , pattern PHexString128
                                        , HexString64 , pattern PHexString64
                                        , HexString32 , pattern PHexString32
                                        , HexString16 , pattern PHexString16
                                        , HexString8 , pattern PHexString8
                                        , RandomGeneratable(..)
                                        , isValidHexChar
                                        ) where

import Shwf.Server.Validation

import Data.Char
import qualified Data.ByteString.UTF8 as BSU

import Data.Aeson
import GHC.Generics

import System.Entropy
import Crypto.Hash


-- reslength must be <= 512
getRandomHexString :: Int -> IO String
getRandomHexString reslength = do
  raw <- getEntropy 512
  return (BSU.toString (BSU.take reslength (digestToHexByteString (hash raw :: Digest SHA512))))

class (Validatable a) => RandomGeneratable a where
  generateRandom :: IO a

isValidHexChar :: Char -> Bool
isValidHexChar x
     | x `elem` ['0'..'9'] = True
     | x `elem` ['a'..'f'] = True
     | x `elem` ['A'..'F'] = True
     | otherwise           = False
{-
isValidHexString :: [Char] -> Bool
isValidHexString x = all isValidHexChar x

onlyValidHexString :: [Char] -> Maybe [Char]
onlyValidHexString x = if (isValidHexString x) then (Just x) else Nothing
-}


--type HexString512 = HexString 512

--newtype HexString (n :: Nat) = HexString String
newtype HexString = HexString String
  deriving          (Generic, Eq)
  deriving anyclass (ToJSON, FromJSON)
instance Validatable HexString where
  -- fromStr always converts the String to upper case, because "6ab1F" should be equivalent "6Ab1f"!
  fromStr str = validate $ HexString $ map toUpper str
  validate resstr@(HexString str) | not (all isValidHexChar str) = Left VErrorInvalidHex
                                  | otherwise = Right resstr
pattern PHexString :: String -> HexString
pattern PHexString str = HexString str
{-# COMPLETE PHexString #-}
instance Show HexString where
  show (PHexString str) = str

------ Fixed HexString 512
newtype HexString512 = HexString512 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString512 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString512 hexstr
  validate res@(PHexString512 hexstr) | len /= 512 = Left $ VErrorWrongSize 512 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString512 :: String -> HexString512
pattern PHexString512 str = HexString512 (HexString str)
{-# COMPLETE PHexString512 #-}
instance RandomGeneratable HexString512 where
  generateRandom = getRandomHexString 512 >>= return . HexString512 . HexString . map toUpper


------ Fixed HexString 256
newtype HexString256 = HexString256 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString256 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString256 hexstr
  validate res@(PHexString256 hexstr) | len /= 256 = Left $ VErrorWrongSize 256 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString256 :: String -> HexString256
pattern PHexString256 str = HexString256 (HexString str)
{-# COMPLETE PHexString256 #-}
instance RandomGeneratable HexString256 where
  generateRandom = getRandomHexString 256 >>= return . HexString256 . HexString . map toUpper

------ Fixed HexString 128
newtype HexString128 = HexString128 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString128 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString128 hexstr
  validate res@(PHexString128 hexstr) | len /= 128 = Left $ VErrorWrongSize 128 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString128 :: String -> HexString128
pattern PHexString128 str = HexString128 (HexString str)
{-# COMPLETE PHexString128 #-}
instance RandomGeneratable HexString128 where
  generateRandom = getRandomHexString 128 >>= return . HexString128 . HexString . map toUpper

------ Fixed HexString 64
newtype HexString64 = HexString64 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString64 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString64 hexstr
  validate res@(PHexString64 hexstr) | len /= 64 = Left $ VErrorWrongSize 64 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString64 :: String -> HexString64
pattern PHexString64 str = HexString64 (HexString str)
{-# COMPLETE PHexString64 #-}
instance RandomGeneratable HexString64 where
  generateRandom = getRandomHexString 64 >>= return . HexString64 . HexString . map toUpper


------ Fixed HexString 32
newtype HexString32 = HexString32 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString32 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString32 hexstr
  validate res@(PHexString32 hexstr) | len /= 32 = Left $ VErrorWrongSize 32 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString32 :: String -> HexString32
pattern PHexString32 str = HexString32 (HexString str)
{-# COMPLETE PHexString32 #-}
instance RandomGeneratable HexString32 where
  generateRandom = getRandomHexString 32 >>= return . HexString32 . HexString . map toUpper

------ Fixed HexString 16
newtype HexString16 = HexString16 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString16 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString16 hexstr
  validate res@(PHexString16 hexstr) | len /= 16 = Left $ VErrorWrongSize 16 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString16 :: String -> HexString16
pattern PHexString16 str = HexString16 (HexString str)
{-# COMPLETE PHexString16 #-}
instance RandomGeneratable HexString16 where
  generateRandom = getRandomHexString 16 >>= return . HexString16 . HexString . map toUpper

------ Fixed HexString 8
newtype HexString8 = HexString8 HexString
  deriving          (Generic, Eq)
  deriving newtype  (Show, ToJSON, FromJSON)
instance Validatable HexString8 where
  fromStr str = case (fromStr str :: Either VError HexString) of
    Left err -> Left err
    Right hexstr -> validate $ HexString8 hexstr
  validate res@(PHexString8 hexstr) | len /= 8 = Left $ VErrorWrongSize 8 len
                                      | otherwise= Right res
                                      where len  = length hexstr
pattern PHexString8 :: String -> HexString8
pattern PHexString8 str = HexString8 (HexString str)
{-# COMPLETE PHexString8 #-}
instance RandomGeneratable HexString8 where
  generateRandom = getRandomHexString 8 >>= return . HexString8 . HexString . map toUpper

