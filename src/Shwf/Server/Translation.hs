module Shwf.Server.Translation ( Language
                               , Translatable (..)
                               , Translation (..)
                               , translation
                               ) where

import Shwf.Server.Validation
import Shwf.Server.Validation.Email
import Shwf.Server.Validation.String

import qualified Data.Text as T
import Data.Char
import Data.Aeson ( FromJSON
                  , ToJSON
                  , FromJSONKey
                  , ToJSONKey
                  , Value(String)
                  , toJSON
                  , parseJSON
                  )
import Data.Aeson.Types ( Parser
                        , withText
                        )
import GHC.Generics

import Data.HashMap.Strict (HashMap, fromList, elems, lookup)

import Data.Hashable

-- valid language tag according to RFC 5646: Tags for Identifying Languages (also known as BCP 47) https://datatracker.ietf.org/doc/html/rfc5646
-- Though here it is just using the 2-Letter ISO 639 code as it is a valid subset of RFC 5646.
-- See ISO 639-1 two-letter codes: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
newtype Language = Language String
  deriving         (Generic)
  -- deriving newtype (Show, Hashable, FromJSON, ToJSON, Eq, FromJSONKey, ToJSONKey)
  deriving newtype (Show, Hashable, Eq, FromJSONKey, ToJSONKey)
instance Validatable Language where
  fromStr str = validate $ Language $ map toUpper str
  validate reslang@(Language str) | not $ all (flip elem ['A'..'Z']) str = Left $ VErrorInvalidChar
                                  | len /= 2 = Left $ VErrorWrongSize 2 len
                                  | otherwise = Right reslang
                                  where
                                    len = length str
instance ToJSON Language where
  toJSON (Language lang) = String $ T.pack $ lang
instance FromJSON Language where
  parseJSON = withText "Language" $ mapLangParser . fromStr . T.unpack
    where
      mapLangParser :: Either VError Language -> Parser Language
      mapLangParser (Left err) = fail $ show err
      mapLangParser (Right lang) = return lang
{-
instance Eq Language where
  (==) (Language a1 b1) (Language a2 b2) = (a1 == a2) && (b1 == b2)
instance ToJSONKey Language
instance FromJSONKey Language
-}

-- langDE :: Language
-- langDE = Language "DE"
-- langEN :: Language
-- langEN = Language "EN"
-- 
-- transDE :: String -> Translation String
-- transDE str = translation $ [(langDE, str)]
-- transEN :: String -> Translation String
-- transEN str = translation $ [(langEN, str)]

-- type TranslationID = String
data Translation a = Translated (HashMap Language a)
                  | Untranslated a
                  -- | Globaltranslated TranslationID
  deriving (Show, Generic, ToJSON, FromJSON)

translation :: [(Language, a)] -> Translation a
translation t = Translated $ fromList t

class Translatable a where
  resolveTranslation :: Language -> Translation a -> String

instance Translatable String where
  resolveTranslation _ (Untranslated str) = str
  resolveTranslation lang (Translated transmap) = case Data.HashMap.Strict.lookup lang transmap of
      Just res -> res
      Nothing -> case elems transmap of
        (x:_) -> x
        [] -> "(Empty Translatable)"
  --resolveTranslation lang (Globaltranslated transid) = return $ "(Globaltranslation not implemented yet: " ++ transid ++ "[" ++ (show lang) ++ "]" ++ ")" -- TODO

instance Translatable RestrictedString where
  resolveTranslation _ (Untranslated str) = show str
  resolveTranslation lang (Translated transmap) = case Data.HashMap.Strict.lookup lang transmap of
      Just res -> show res
      Nothing -> case elems transmap of
        (x:_) -> show x
        [] -> "(Empty Translatable)"

instance Translatable Email where
  resolveTranslation _ (Untranslated str) = show str
  resolveTranslation lang (Translated transmap) = case Data.HashMap.Strict.lookup lang transmap of
      Just res -> show res
      Nothing -> case elems transmap of
        (x:_) -> show x
        [] -> "(Empty Translatable)"


