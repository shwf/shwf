-- {-#LANGUAGE FlexibleContexts #-}
-- {-#LANGUAGE FlexibleInstances #-}
-- {-#LANGUAGE OverloadedStrings #-}
-- {-#LANGUAGE TupleSections #-}
-- {-#LANGUAGE TypeSynonymInstances #-}
-- {-#LANGUAGE MultiParamTypeClasses #-}
-- {-#LANGUAGE ScopedTypeVariables #-}
-- {-#LANGUAGE LambdaCase #-}

module Shwf.Server.Templates ( Template
                             , TemplateName
                             , Templates
                             , TemplateErrors
                             , renderTemplate
                             , printParseErrors
                             , parseTemplates
                             , lookupTemplate
                             ) where

import Shwf.Server.FormattedText
import Shwf.Server.Translation

import Text.Ginger (
                   easyRender
                   ,SourcePos
                   ,SourceName
                   ,ParserError(..)
                   ,Statement(NullS)
                   ,parseGingerFile
                   ,formatParserError
                   ,Run
                   ,rawJSONToGVal
                   ,VarName
                   )
import Text.Ginger.GVal ( GVal(..)
                        , ToGVal(..)
                        , asHashMap
                        , toGVal
                        , fromFunction
                        , Function
                        )
import Text.Ginger.Html
import qualified Text.Ginger.AST (Template (..))

import Text.Parsec.Pos (initialPos)

import qualified Data.HashMap.Strict as HashMap
import Data.HashMap.Strict (HashMap, elems, union, fromList)
import Data.Text (Text, pack)
import Data.Aeson ( Result(..)
                  , toJSON
                  , ToJSON
                  , fromJSON
                  , FromJSON
                  )

import Control.Monad.Writer

--import Text.Blaze.Html.Renderer.Pretty (renderHtml)


--import System.Directory  (listDirectory)
--import System.IO (IOMode(ReadMode), openFile, hGetContents)
--import Data.List (isPrefixOf)
import Data.List.Split



type Template = Text.Ginger.AST.Template SourcePos
type TemplateName = SourceName
type Templates = HashMap TemplateName Template
type TemplateErrors = [(Maybe String, ParserError)]

decodeGVal :: FromJSON a => GVal m -> Result a
decodeGVal g = fromJSON $ toJSON $ g
--decodeGVal g = case fromGValEither g :: Either String Value of
--    Right v -> fromJSON v
--    Left err -> Error err

defaultScope :: HashMap VarName (GVal (Run SourcePos (Writer Text) Text))
defaultScope = fromList $
    [ ("translate", fromFunction gfnTranslate)
    --, ("renderInput", fromFunction gfnRenderInput)
    --, ("testtranslation", rawJSONToGVal $ toJSON (translation [(langDE, "Guten <strong>\"Tag\"</strong>."), (langEN, "Good <i>morning</i>.")] :: Translation String))
    ]

-- translate(translatable, lang, [escape :: Bool])
gfnTranslate :: Monad m => Function m
gfnTranslate [(_, rawt), (_, rawlang), (_, rawescape)] = do
  case decodeGVal rawlang :: Result (Language) of
    Error err -> return $ rawJSONToGVal $ toJSON $ ("Failed to parse lang: " ++ err)
    Success lang -> case decodeGVal rawt :: Result (Translation [FormattedText]) of
      Success tft -> return $ gfnTranslate' (resolveTranslation lang tft) (asBoolean rawescape)
      Error _ -> case decodeGVal rawt :: Result (Translation String) of
        Error err -> return $ rawJSONToGVal $ toJSON err
        Success tstr -> return $ gfnTranslate' (resolveTranslation lang tstr) (asBoolean rawescape)
  where
    gfnTranslate' :: String -> Bool -> GVal m
    gfnTranslate' str False = toGVal $ pack $ str
    gfnTranslate' str True = toGVal $ html $ asText $ gfnTranslate' str False -- Escape
gfnTranslate (rawt:rawlang:_) = gfnTranslate [rawt, rawlang, (Just "escape", toGVal True)]
gfnTranslate _ = return $ "ERROR: Wrong arguments for translate()."

-- gfnRenderInput :: Monad m => Function m
-- gfnRenderInput [(_, rawinput), (_, rawlang)] = do
--   case decodeGVal rawlang :: Result (Language) of
--     Error err -> return $ rawJSONToGVal $ toJSON err
--     Success lang -> case decodeGVal rawinput :: Result (Input) of
--       Error err -> return $ rawJSONToGVal $ toJSON err
--       Success inp -> return $ toGVal $ renderHtml $ renderInputBody lang inp
-- gfnRenderInput _ = return $ "ERROR: Wrong arguments for renderInput()."

mergeGValDicts :: GVal m -> GVal m -> GVal m
mergeGValDicts a b = toGVal $ mergeGValDicts' (asHashMap a) (asHashMap b)
  where
    mergeGValDicts' :: Maybe (HashMap Text (GVal m)) ->  Maybe (HashMap Text (GVal m))  ->  HashMap Text (GVal m)
    mergeGValDicts' (Just a') (Just b') = union a' b'
    mergeGValDicts' (Just a') Nothing = a'
    mergeGValDicts' Nothing (Just b') = b'
    mergeGValDicts' Nothing Nothing = fromList []



printParseErrors :: TemplateErrors -> IO ()
printParseErrors [(templateSource, err)] = putStrLn ("\ESC[31m" ++ "ERROR: " ++ (formatParserError templateSource err) ++ "\ESC[0m")
printParseErrors (err:errs) = do
  printParseErrors [err]
  printParseErrors errs
printParseErrors _ = putStrLn "No errors."




parseTemplates :: HashMap FilePath String -> Either TemplateErrors Templates
parseTemplates templatesDir = if (null errors)
  then (Right templates)
  else (Left errors)
  where
    templates :: Templates
    templates = HashMap.map (either (\_ -> (Text.Ginger.AST.Template (NullS (initialPos "Should never happen.")) HashMap.empty Nothing)) (\x -> x)) rawTemplates

    errors :: TemplateErrors
    errors = elems (HashMap.mapWithKey errorsMapping errors')
    errors' = HashMap.mapMaybe (either (\x -> Just x) (\_ -> Nothing)) rawTemplates
    errorsMapping templateName err = (getFile templateName templatesDir, err)

    rawTemplates :: HashMap TemplateName (Either ParserError Template)
    rawTemplates = HashMap.mapWithKey (parseTemplate templatesDir) templatesDir

parseTemplate :: HashMap FilePath String -> TemplateName -> String -> Either ParserError Template
parseTemplate templatesDir fn _ = parseTemplate' (parseGingerFile (getTemplateFile templatesDir) fn)
  where
    parseTemplate' :: Maybe (Either ParserError Template) -> Either ParserError Template
    parseTemplate' Nothing = Left (ParserError "parseGingerFile returned Nothing. This shouldn't happen." Nothing)
    parseTemplate' (Just template) = template

    getTemplateFile :: HashMap FilePath String -> FilePath -> Maybe (Maybe String)
    getTemplateFile templatesDir' fn' = Just (getFile fn' templatesDir')


lookupTemplate :: TemplateName -> Templates -> Maybe Template
lookupTemplate templateName templates = HashMap.lookup (filename templateName) templates



-- https://github.com/tdammers/ginger/issues/65
renderTemplateText :: ToJSON c => Template -> c -> Either Text Text
renderTemplateText template ctx = do
  let ctxGVal :: GVal (Run SourcePos (Writer Text) Text) = rawJSONToGVal $ toJSON ctx
  let defctxGVal :: GVal (Run SourcePos (Writer Text) Text) = toGVal defaultScope
  let r :: Text = easyRender (mergeGValDicts ctxGVal defctxGVal) template
  return r

renderTemplate :: ToJSON c => Template -> c -> Either Text Html
renderTemplate template ctx = convertRightToHtml (renderTemplateText template ctx)
  where
    convertRightToHtml (Left err) = Left err
    convertRightToHtml (Right rendered) = Right (unsafeRawHtml rendered)


-----------------------------
---- File Helper
-----------------------------

filename :: FilePath -> FilePath
filename fn = last (splitOn "/" fn)

getFile :: FilePath -> HashMap FilePath String -> Maybe String
getFile fn dir = HashMap.lookup (filename fn) dir

--readDir :: FilePath -> IO (HashMap FilePath String)
--readDir dirpath = do
--  fns' <- listDirectory dirpath -- get files in directory
--  let fns = filter (not . isPrefixOf ".") fns' -- remove all files beginning with '.'
--  contents <- loadFiles (map ((++) (dirpath ++ "/")) fns)
--  return (fromList (zip fns contents))

--loadFiles :: [FilePath] -> IO [String]
--loadFiles fns = mapM loadFile fns
--
--loadFile :: FilePath -> IO String
--loadFile fn = do
--  f <- openFile fn ReadMode
--  hGetContents f


