# Simple Haskell Web Framework

[![pipeline status](https://gitlab.gwdg.de/shwf/shwf/badges/main/pipeline.svg)](https://gitlab.gwdg.de/shwf/shwf/-/commits/main) 

## Dokumentation

https://shwf.pages.gwdg.de/shwf/html/shwf/

## Abhängigkeiten installieren

Allgemein:
```bash
sudo apt update
sudo apt purge ghc cabal-install
sudo apt install make zlib1g-dev redis build-essential curl libffi-dev libffi8ubuntu1 libgmp-dev libgmp10 libncurses-dev libncurses5 libtinfo5
curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
```

```
$ ghci --version
The Glorious Glasgow Haskell Compilation System, version 9.2.5
$ cabal --version
cabal-install version 3.6.2.0
compiled using version 3.6.2.0 of the Cabal library
```


Im Repo:
```bash
cabal update
cabal install
make
```

## Links

- https://hackage.haskell.org/package/happstack-lite-7.3.8/docs/Happstack-Lite.html
- https://www.happstack.com/page/view-page-slug/9/happstack-lite
- https://www.happstack.com/docs/crashcourse/index.html



