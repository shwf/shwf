module Main (main) where

import Shwf.Server.Config

import Control.Applicative ((<$>), optional)
import Data.Maybe (fromMaybe)
--import Data.Text (Text)
--import Data.Text.Lazy (unpack)
import Happstack.Lite
import Happstack.Server (result, askRq)
--import Text.Blaze.Html5 (Html, (!), a, form, input, p, toHtml, label)
--import Text.Blaze.Html5.Attributes (action, enctype, href, name, size, type_, value)
--import qualified Text.Blaze.Html5 as H
--import qualified Text.Blaze.Html5.Attributes as A


main :: IO ()
main = do
  config <- readConfigFile "test/config.json"
  print config
  serve Nothing myApp

myApp :: ServerPart Response
myApp = do
  req <- askRq
  let res = show req
  return $ result 200 res
